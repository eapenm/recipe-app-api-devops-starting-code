output "db_host" {
  value = aws_db_instance.main.address
}

output "bastion_host" {
  value = aws_instance.bastion.public_dns
}
# Output variable for route53 fully qualified dns name once it's been created by terraform.
output "api_endpoint" {
  value = aws_route53_record.app.fqdn
}