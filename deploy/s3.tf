resource "aws_s3_bucket" "app_public_files" {
  bucket = "${local.prefix}-files"
  acl    = "public-read"
  # This is to give terraform access to destroy the S3 bucket when we teardown the environment
  force_destroy = true

}