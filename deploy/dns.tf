data "aws_route53_zone" "zone" {
  name = "${var.dns_zone_name}."
}

resource "aws_route53_record" "app" {
  zone_id = data.aws_route53_zone.zone.zone_id
  name    = "${lookup(var.subdomain, terraform.workspace)}.${data.aws_route53_zone.zone.name}"
  type    = "CNAME"
  ttl     = "300"
  records = [aws_lb.api.dns_name]

}

# create certificate using amazon certificate manager
resource "aws_acm_certificate" "cert" {
  # fqdn - fully qualified domain name
  domain_name = aws_route53_record.app.fqdn
  # validate if the domain name is owned by you or not
  validation_method = "DNS"
  tags              = local.common_tags
  lifecycle {
    create_before_destroy = true
  }
}

# domain validation
resource "aws_route53_record" "cert_validation" {
  name    = aws_acm_certificate.cert.domain_validation_options.0.resource_record_name
  type    = aws_acm_certificate.cert.domain_validation_options.0.resource_record_type
  zone_id = data.aws_route53_zone.zone.zone_id
  records = [
    aws_acm_certificate.cert.domain_validation_options.0.resource_record_value
  ]
  ttl = "60"
}

# this will trigger the aws certification validation
resource "aws_acm_certificate_validation" "cert" {
  certificate_arn         = aws_acm_certificate.cert.arn
  validation_record_fqdns = aws_route53_record.cert_validation.fqdn

}