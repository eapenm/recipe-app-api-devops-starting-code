variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "eapenmani@gmail.com"
}

variable "db_username" {
  description = "Database Username for RDS postgress database"
}

variable "db_password" {
  description = "Database password for postgress database"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"

}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "560321588640.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest"
}
variable "django_secret_key" {
  description = "Secret key for Django app"
}

variable "ecr_image_proxy" {
  description = "ECR Image for API"
  default     = "560321588640.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy:latest"
}
# ... existing code ...

variable "dns_zone_name" {
  description = "Domain name"
  default     = "<domain name>"
}

variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}
