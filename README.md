# Recipe Application
Technology Used to build this application.

- Language: 
    - Python
    - Django / Django-REST-Framework
- Container Technology: Docker, ECR , ECS
- IaC: Terraform 
- CI/CD : GitLab
- Cloud Environment: AWS 


#### Note: I did not develop the application using Python and Django. I used the application to create the docker and deploy the application in aws ECR and ECS using terrform scripts.


### Architecture Diagram:

![Receipe_architecture_diagram](https://gitlab.com/eapenm/recipe-app-api-devops/-/blob/master/Receipe_architecture_diagram.jpg)

